### Descrição
Este pedido de merge aborda e descreve o problema ou história do usuário que está sendo tratado.

### Alterações Realizadas
Forneça trechos de código ou capturas de tela conforme necessário.

### Problemas Relacionados
Forneça links para os problemas ou solicitações de funcionalidades relacionados.

### Notas Adicionais
Inclua qualquer informação extra ou considerações para os revisores, como áreas impactadas no código-fonte.

### Listas de Verificação do Pedido de Merge
- [ ] O código segue as diretrizes de codificação do projeto.
- [ ] A documentação reflete as alterações realizadas.
- [ ] Já cobri os testes unitários.

### Issue referenciada

Closes <link-da-issue>